package com.cbk.judo.shake.ordering.mvp.presenter;

public interface BasePresenter {
    void onDestroy();
}
