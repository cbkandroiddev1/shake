package com.cbk.judo.shake.ordering.mvp.view;

public interface BaseView {

    void onError(String message);
}
