package com.cbk.judo.shake.ordering.mvp.presenter;

public interface LoginPresenter extends BasePresenter {

    void onClickLogin(String email, String password);
}
