package com.cbk.judo.shake.ordering.mvp.model;

public interface ApiHelper {

    void loginAccount(String email, String password, OnFinishedListener finishedListener);

    interface OnFinishedListener{
        void onFinished();
        void onError(String message);
    }
}
