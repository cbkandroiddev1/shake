package com.cbk.judo.shake.ordering.mvp.presenter;

import android.util.Log;

import com.cbk.judo.shake.ordering.mvp.model.ApiHelper;
import com.cbk.judo.shake.ordering.mvp.view.LoginView;

public class LoginPresenterImpl implements LoginPresenter, ApiHelper.OnFinishedListener {

    private LoginView loginView;
    private ApiHelper apiHelper;

    public LoginPresenterImpl(LoginView loginView, ApiHelper apiHelper){
        this.loginView = loginView;
        this.apiHelper = apiHelper;
    }

    @Override
    public void onClickLogin(String email, String password) {
        loginView.showProgress();
        apiHelper.loginAccount(email, password, this);
    }

    @Override
    public void onFinished() {
        loginView.hideProgress();
    }

    @Override
    public void onError(String message) {
        loginView.hideProgress();
        loginView.onError(message);
    }

    @Override
    public void onDestroy() {
        if (loginView != null){
            loginView = null;
        }
    }
}
