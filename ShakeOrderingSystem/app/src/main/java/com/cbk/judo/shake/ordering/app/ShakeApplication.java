package com.cbk.judo.shake.ordering.app;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.view.View;
import android.widget.Toast;

import com.cbk.judo.shake.ordering.R;

public class ShakeApplication extends Application {

    private static SharedPreferences sharedPreference;
    private static final String PREFERENCE_KEY = "shake_pref";
    private static Context context;

    @Override
    public void onCreate() {
        super.onCreate();
        context = this;
        sharedPreference = getSharedPreferences(PREFERENCE_KEY, MODE_PRIVATE);
    }

    private static String getStringPreferenceValue(String key) {
        return sharedPreference.getString(key, "");
    }

    private static boolean getBooleanPreferenceValue(String key) {
        return sharedPreference.getBoolean(key, false);
    }

    private static void showToast(Context context, String message) {
        Toast toast = Toast.makeText(context, message, Toast.LENGTH_LONG);
        View view1 = toast.getView();
        toast.getView().setPadding(20, 20, 20, 20);
        view1.setBackgroundResource(R.color.primary_dark);
        toast.show();
    }

    public static void showToast(String message){
        showToast(context, message);
    }

    private static void saveToSharedPreference(String key, String value){
        sharedPreference.edit().putString(key,value).commit();
    }

    private static void saveToSharedPreference(String key, boolean value) {
        sharedPreference.edit().putBoolean(key, value).commit();
    }
}
