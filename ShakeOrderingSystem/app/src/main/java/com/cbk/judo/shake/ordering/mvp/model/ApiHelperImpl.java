package com.cbk.judo.shake.ordering.mvp.model;

import com.cbk.judo.shake.ordering.mvp.data.WebResponse;
import com.cbk.judo.shake.ordering.retrofit.RetrofitFactory;
import com.cbk.judo.shake.ordering.retrofit.ShakeApiService;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class ApiHelperImpl implements ApiHelper {
    private static final String BASE_URL = "https://demo.judosoft.com/judo-dms/";

    @Override
    public void loginAccount(String email, String password, final OnFinishedListener finishedListener) {
        ShakeApiService shakeApiService = RetrofitFactory.getService(ShakeApiService.class, BASE_URL);
        shakeApiService.authenticateUserWithEmail(email, password)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<WebResponse>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onSuccess(WebResponse webResponse) {
                        finishedListener.onFinished();
                    }

                    @Override
                    public void onError(Throwable e) {
                        finishedListener.onError(e.getMessage());
                    }
                });
    }
}
