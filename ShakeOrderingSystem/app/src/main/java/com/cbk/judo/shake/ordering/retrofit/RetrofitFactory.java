package com.cbk.judo.shake.ordering.retrofit;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public final class RetrofitFactory {

    // don't let to create object
    private RetrofitFactory(){
    }

    private static Retrofit retrofit;

    private static Retrofit getRetrofit(String baseUrl){
        if (retrofit==null){
            // create OkHttpClient
            OkHttpClient okHttpClient = new OkHttpClient.Builder()
                    .addInterceptor(new Interceptor() {
                        @Override
                        public Response intercept(Chain chain) throws IOException {
                            Request originRequest = chain.request();
                            Request.Builder builder = originRequest.newBuilder();
                            Request newRequest = builder.build();
                            Response response = chain.proceed(newRequest);
                            return response;
                        }
                    }).build();

            Gson gson = new GsonBuilder().setLenient().create();

            // create retrofit
            retrofit = new Retrofit.Builder()
                    .baseUrl(baseUrl)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .client(okHttpClient)
                    .build();
        }
        return retrofit;
    }

    public static <T> T getService(Class<T> service, String baseUrl){
        return getRetrofit(baseUrl).create(service);
    }
}
