package com.cbk.judo.shake.ordering.mvp.view;

public interface LoginView extends BaseView {
    void showProgress();
    void hideProgress();
}
