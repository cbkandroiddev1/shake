package com.cbk.judo.shake.ordering.activities;

import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;

import com.cbk.judo.shake.ordering.R;
import com.cbk.judo.shake.ordering.mvp.model.ApiHelperImpl;
import com.cbk.judo.shake.ordering.mvp.presenter.LoginPresenter;
import com.cbk.judo.shake.ordering.mvp.presenter.LoginPresenterImpl;
import com.cbk.judo.shake.ordering.mvp.view.LoginView;

import androidx.annotation.Nullable;

public class LoginActivity extends BaseActivity implements LoginView {

    private LoginPresenter loginPresenter;

    private EditText etEmail, etPassword;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        loginPresenter = new LoginPresenterImpl(this, new ApiHelperImpl());

        fetchViews();
    }

    private void fetchViews() {
        etEmail = findViewById(R.id.et_email);
        etPassword = findViewById(R.id.et_password);
    }

    public void login(View view) {
        hideKeyboard();

        String email = etEmail.getText().toString().trim();
        String password = etPassword.getText().toString().trim();
        if (checkValidate(email, password)) {
            loginPresenter.onClickLogin(email, password);
        }
    }

    private boolean checkValidate(String email, String password) {
        if (email.isEmpty() || password.isEmpty()) {
            if (email.isEmpty()) {
                //setting error message
                String param = getString(R.string.email);
                String errorMessage = getString(R.string.error_require,param);
                etEmail.setError(errorMessage);

            }else if (password.isEmpty()) {
                //setting error message
                String param = getString(R.string.password);
                String errorMessage = getString(R.string.error_require,param);
                etPassword.setError(errorMessage);
            }
            return false;
        }
        return true;
    }

    @Override
    public void showProgress() {
        showLoading();
    }

    @Override
    public void hideProgress() {
        hideLoading();
    }

    @Override
    public void onError(String message) {
        showErrorMessage(message);
    }
}
