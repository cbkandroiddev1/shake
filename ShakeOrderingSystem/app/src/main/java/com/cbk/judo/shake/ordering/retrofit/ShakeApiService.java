package com.cbk.judo.shake.ordering.retrofit;

import com.cbk.judo.shake.ordering.mvp.data.WebResponse;

import io.reactivex.Single;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface ShakeApiService {

    @POST("api/login/loginUser/")
    Single<WebResponse> authenticateUserWithEmail(@Query("userName") String username, @Query("password") String password);
}
