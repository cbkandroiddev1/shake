package com.cbk.judo.shake.ordering.activities;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cbk.judo.shake.ordering.R;
import com.cbk.judo.shake.ordering.database.ShakeDB;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

public class BaseActivity extends AppCompatActivity {

    private ShakeDB shakeDB;
    private AlertDialog loadingDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    protected ShakeDB getDatabaseInstance(){
        if (shakeDB == null) {
            shakeDB = new ShakeDB(this);
        }
        return shakeDB;
    }

    protected void showLoading(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(R.layout.dialog_loading);
        builder.setCancelable(false);
        loadingDialog = builder.create();
        loadingDialog.show();
    }

    protected void hideLoading(){
        if (loadingDialog != null && loadingDialog.isShowing()) {
            loadingDialog.hide();
        }
    }

    protected void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager)getSystemService(INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(this.getCurrentFocus().getWindowToken(), 0);
    }

    protected void showErrorMessage(String message){
        if (message == null)
            return;
        if (message.contains("HTTP 403")) {
            showErrorDialog("Access Denied!");
        }else if (message.contains("HTTP 503")) {
            showErrorDialog("Sorry! Server is Under Maintenance!");
        } else if(message.equals("timeout")){
            showErrorDialog("Sorry! Connection is too slow!");
        }else if (message.contains("HTTP 406")) {
            showErrorDialog("Please Sing Up!");
        }else if (message.contains("HTTP 423")) {
            showErrorDialog("Sorry! Your Account has been locked!");
        }else {
            showErrorDialog("Sorry! Cannot connect to server!");
        }
    }

    private void showErrorDialog(String infoMsg) {
        final Dialog dialog = new Dialog(this, R.style.AppTheme);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        WindowManager.LayoutParams params = getWindow().getAttributes();
        params.x = 0;
        params.y = 0;
        params.gravity = Gravity.TOP | Gravity.LEFT;
        dialog.setContentView(R.layout.dialog_info);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);

        TextView tvInfo = dialog.findViewById(R.id.tvinfo);
        tvInfo.setText(infoMsg);
        Button btnOk = dialog.findViewById(R.id.btnOk);

        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }
}
